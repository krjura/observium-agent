**EOL** - For a while now I am not using Observium for monitoring purposes and not actively developing this code. 
If you still want to use it your welcome.


Project structure:

* **src/main/java** - Java code of the JVM agent
* **src/main/php** - PHP code for observium poller and graph system
* **etc/bin** - Bash scripts for running the agent
* **etc/conf** - configuration file
* **build.gradle** - Gradle script for building the project

to build the project run gradle clean productionDistZip. Please note that project has Java 6 compatibility. If you are running new version of Java on your PC uncomment this code:


```
#!text
def bootClasspathStr = "c:/ProgramFiles/jdk1.6.0_29/jre/lib/rt.jar"
project.tasks.withType(AbstractCompile, { AbstractCompile ac ->
    ac.options.bootClasspath = bootClasspathStr // options is always there but not defined on AbstractCompile so going to hit it anyway
})
```

and set bootClasspathStr to your directory path.
