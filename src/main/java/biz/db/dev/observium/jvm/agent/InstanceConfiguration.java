package biz.db.dev.observium.jvm.agent;

public class InstanceConfiguration {

	private String name;

	private String hostname;

	private String port;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("InstanceConfiguration{");
		sb.append("name='").append(name).append('\'');
		sb.append(", hostname='").append(hostname).append('\'');
		sb.append(", port='").append(port).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
