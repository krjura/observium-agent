package biz.db.dev.observium.jvm.agent;

import com.sun.management.ThreadMXBean;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.JMX;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerDelegateMBean;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.lang.management.ClassLoadingMXBean;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.RuntimeMXBean;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class ObserviumJvmUnixAgent {

    private static final Logger LOG = Logger.getLogger( ObserviumJvmUnixAgent.class.getName() );

    private String jmxUrl = null;
    private String name = null;

    private JMXConnector jmxc;
    private MBeanServerConnection connection;

    private static final String MEMORY_POOL_PREFIX = "java.lang:type=MemoryPool,name=";
    private String edenSpacePoolName = "java.lang:type=MemoryPool,name=Eden Space";
    private String permGenPoolName = "java.lang:type=MemoryPool,name=Perm Gen";
    private String oldGenPoolName = "java.lang:type=MemoryPool,name=Tenured Gen";

    public ObserviumJvmUnixAgent(String jmxUrl, String name) {
        this.jmxUrl = jmxUrl;
        this.name = name;
    }

    public void init() throws IOException {
        JMXServiceURL serviceURL = new JMXServiceURL(jmxUrl);
        jmxc = JMXConnectorFactory.connect(serviceURL, null);
        connection = jmxc.getMBeanServerConnection();

        LOG.info("initialized ObserviumJvmUnixAgent with name=" + name + " and url=" + jmxUrl);
    }

    public void close() {
        try {
            jmxc.close();
        } catch(IOException ex) {
            LOG.log(Level.WARNING, "ex", ex);
        }
    }

    public void printHeader() {
        System.out.println("\n\n<<<app-jvmoverjmx-" + this.name + ">>>");
    }

    private void checkConfiguration() {

        String[] InputArguments;

        try {
            ObjectName runtimeObjectName = new ObjectName("java.lang:type=Runtime");
            InputArguments = (String[]) connection.getAttribute(runtimeObjectName, "InputArguments");

            for(String argument : InputArguments) {
                // G1 garbage Collector has different names for memory pools; Change default

                ObjectName gcObjectName;
                String[] memoryPoolNames;

                if( argument.equals("-XX:+UseG1GC") ) {
                    gcObjectName = new ObjectName("java.lang:type=GarbageCollector,name=G1 Old Generation");
                    memoryPoolNames = (String[]) connection.getAttribute(gcObjectName, "MemoryPoolNames");

                    this.edenSpacePoolName = MEMORY_POOL_PREFIX + memoryPoolNames[0];
                    this.oldGenPoolName = MEMORY_POOL_PREFIX + memoryPoolNames[2];
                    this.permGenPoolName = MEMORY_POOL_PREFIX + memoryPoolNames[3];
                } else if( argument.equals("-XX:+UseConcMarkSweepGC") ) {
                    gcObjectName = new ObjectName("java.lang:type=GarbageCollector,name=ConcurrentMarkSweep");
                    memoryPoolNames = (String[]) connection.getAttribute(gcObjectName, "MemoryPoolNames");

                    this.edenSpacePoolName = MEMORY_POOL_PREFIX + memoryPoolNames[0];
                    this.oldGenPoolName = MEMORY_POOL_PREFIX + memoryPoolNames[2];
                    this.permGenPoolName = MEMORY_POOL_PREFIX + memoryPoolNames[3];
                }  else if( argument.equals("-XX:+UseParallelOldGC") ) {
                    gcObjectName = new ObjectName("java.lang:type=GarbageCollector,name=PS MarkSweep");
                    memoryPoolNames = (String[]) connection.getAttribute(gcObjectName, "MemoryPoolNames");

                    this.edenSpacePoolName = MEMORY_POOL_PREFIX + memoryPoolNames[0];
                    this.oldGenPoolName = MEMORY_POOL_PREFIX + memoryPoolNames[2];
                    this.permGenPoolName = MEMORY_POOL_PREFIX + memoryPoolNames[3];
                }
            }
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }
    }

    public void printMemoryInfo() {
        long heapMemoryMaxUsage = 0l;
        long heapMemoryUsed = 0l;
        long nonHeapMemoryMaxUsage = 0l;
        long nonHeapMemoryUsed = 0l;

        try {
            ObjectName memoryObjectName = new ObjectName("java.lang:type=Memory");
            CompositeDataSupport heapMemoryComposite = (CompositeDataSupport) connection.getAttribute(memoryObjectName, "HeapMemoryUsage");
            CompositeDataSupport nonHeapMemoryComposite = (CompositeDataSupport) connection.getAttribute(memoryObjectName, "NonHeapMemoryUsage");

            heapMemoryMaxUsage = (Long) heapMemoryComposite.get("max");
            heapMemoryUsed = (Long) heapMemoryComposite.get("used");
            nonHeapMemoryMaxUsage = (Long) nonHeapMemoryComposite.get("max");
            nonHeapMemoryUsed = (Long) nonHeapMemoryComposite.get("used");
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // heap usage
        System.out.println(String.format("%-30s : %-30s" ,"HeapMemoryMaxUsage",heapMemoryMaxUsage));
        System.out.println(String.format("%-30s : %-30s" ,"HeapMemoryUsed",heapMemoryUsed));

        // non heap usage
        System.out.println(String.format("%-30s : %-30s" ,"NonHeapMemoryMax",nonHeapMemoryMaxUsage));
        System.out.println(String.format("%-30s : %-30s" ,"NonHeapMemoryUsed",nonHeapMemoryUsed));
    }

    public void printEdenSpace() {

        long edenSpaceMaxUsage = 0l;
        long edenSpaceUsed = 0l;

        try {
            ObjectName edenSpaceObjectName = new ObjectName(edenSpacePoolName);
            CompositeDataSupport edenSpaceComposite = (CompositeDataSupport) connection.getAttribute(edenSpaceObjectName, "Usage");

            edenSpaceMaxUsage = (Long) edenSpaceComposite.get("max");
            edenSpaceUsed = (Long) edenSpaceComposite.get("used");
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // eden space
        System.out.println(String.format("%-30s : %-30s" ,"EdenSpaceMax",edenSpaceMaxUsage));
        System.out.println(String.format("%-30s : %-30s" ,"EdenSpaceUsed",edenSpaceUsed));
    }

    public void printPermGen() {
        long permGenMaxUsage = 0l;
        long permGenUsed = 0l;

        try {
            ObjectName permGenObjectName = new ObjectName(permGenPoolName);
            CompositeDataSupport permGenComposite = (CompositeDataSupport) connection.getAttribute(permGenObjectName, "Usage");

            permGenMaxUsage = (Long) permGenComposite.get("max");
            permGenUsed = (Long) permGenComposite.get("used");
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // perm gen
        System.out.println(String.format("%-30s : %-30s" ,"PermGenMax",permGenMaxUsage));
        System.out.println(String.format("%-30s : %-30s" ,"PermGenUsed",permGenUsed));
    }

    public void printOldGeneration() {
        long oldGenerationMaxUsage = 0l;
        long oldGenerationUsed = 0l;

        try {
            ObjectName oldGenerationObjectName = new ObjectName(oldGenPoolName);
            CompositeDataSupport oldGenerationComposite = (CompositeDataSupport) connection.getAttribute(oldGenerationObjectName, "Usage");

            oldGenerationMaxUsage = (Long) oldGenerationComposite.get("max");
            oldGenerationUsed = (Long) oldGenerationComposite.get("used");
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // old generation
        System.out.println(String.format("%-30s : %-30s" ,"OldGenMax",oldGenerationMaxUsage));
        System.out.println(String.format("%-30s : %-30s" ,"OldGenUsed",oldGenerationUsed));
    }

    private void printG1OldGenerationInfo() {
        long g1OldGenCollectionCount = 0l;
        long g1OldGenCollectionTime = 0l;

        try {
            ObjectName g1OldGeneration = new ObjectName("java.lang:type=GarbageCollector,name=G1 Old Generation");
            GarbageCollectorMXBean g1OldGenerationMxBean = JMX.newMBeanProxy(connection, g1OldGeneration, GarbageCollectorMXBean.class, true);

            g1OldGenCollectionCount = g1OldGenerationMxBean.getCollectionCount();
            g1OldGenCollectionTime = g1OldGenerationMxBean.getCollectionTime();
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // G1
        System.out.println(String.format("%-30s : %-30s" ,"G1OldGenCollectionCount", g1OldGenCollectionCount));
        System.out.println(String.format("%-30s : %-30s" ,"G1OldGenCollectionTime", g1OldGenCollectionTime));
    }

    private void printG1YoungGenerationInfo() {
        long g1YoungGenCollectionCount = 0l;
        long g1YoungGenCollectionTime = 0l;

        try {
            ObjectName g1YoungGeneration = new ObjectName("java.lang:type=GarbageCollector,name=G1 Young Generation");
            GarbageCollectorMXBean g1YoungGenerationMxBean = JMX.newMBeanProxy(connection, g1YoungGeneration, GarbageCollectorMXBean.class, true);

            g1YoungGenCollectionCount = g1YoungGenerationMxBean.getCollectionCount();
            g1YoungGenCollectionTime = g1YoungGenerationMxBean.getCollectionTime();
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // G1
        System.out.println(String.format("%-30s : %-30s" ,"G1YoungGenCollectionCount", g1YoungGenCollectionCount));
        System.out.println(String.format("%-30s : %-30s" ,"G1YoungGenCollectionTime", g1YoungGenCollectionTime));
    }

    private void printConcMarkSweepInfo() {
        long cmsCollectionCount = 0l;
        long cmsCollectionTime = 0l;

        try {
            ObjectName cmsObjectName = new ObjectName("java.lang:type=GarbageCollector,name=ConcurrentMarkSweep");
            GarbageCollectorMXBean cmsGenerationMxBean = JMX.newMBeanProxy(connection, cmsObjectName, GarbageCollectorMXBean.class, true);

            cmsCollectionCount = cmsGenerationMxBean.getCollectionCount();
            cmsCollectionTime = cmsGenerationMxBean.getCollectionTime();
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // Concurrent Mark Sweep (CMS) Collector
        System.out.println(String.format("%-30s : %-30s" ,"CMSCollectionCount", cmsCollectionCount));
        System.out.println(String.format("%-30s : %-30s" ,"CMSCollectionTime", cmsCollectionTime));
    }

    private void printParNewInfo() {
        long parNewCollectionCount = 0l;
        long parNewCollectionTime = 0l;

        try {
            ObjectName parNewObjectName = new ObjectName("java.lang:type=GarbageCollector,name=ParNew");
            GarbageCollectorMXBean parNewGenerationMxBean = JMX.newMBeanProxy(connection, parNewObjectName, GarbageCollectorMXBean.class, true);

            parNewCollectionCount = parNewGenerationMxBean.getCollectionCount();
            parNewCollectionTime = parNewGenerationMxBean.getCollectionTime();
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // Par New
        System.out.println(String.format("%-30s : %-30s" ,"ParNewCollectionCount", parNewCollectionCount));
        System.out.println(String.format("%-30s : %-30s" ,"ParNewCollectionTime", parNewCollectionTime));
    }

    private void printCopyInfo() {
        long copyCollectionCount = 0l;
        long copyCollectionTime = 0l;

        try {
            ObjectName copyNewObjectName = new ObjectName("java.lang:type=GarbageCollector,name=Copy");
            GarbageCollectorMXBean copyGenerationMxBean = JMX.newMBeanProxy(connection, copyNewObjectName, GarbageCollectorMXBean.class, true);

            copyCollectionCount = copyGenerationMxBean.getCollectionCount();
            copyCollectionTime = copyGenerationMxBean.getCollectionTime();
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // Copy
        System.out.println(String.format("%-30s : %-30s" ,"CopyCollectionCount", copyCollectionCount));
        System.out.println(String.format("%-30s : %-30s" ,"CopyCollectionTime", copyCollectionTime));
    }

    private void printPSMarkSweepInfo() {
        long markSweepCollectionCount = 0l;
        long markSweepCollectionTime = 0l;

        try {
            ObjectName  markSweepObjectName = new ObjectName("java.lang:type=GarbageCollector,name=PS MarkSweep");
            GarbageCollectorMXBean  markSweepGenerationMxBean = JMX.newMBeanProxy(connection, markSweepObjectName, GarbageCollectorMXBean.class, true);

            markSweepCollectionCount = markSweepGenerationMxBean.getCollectionCount();
            markSweepCollectionTime = markSweepGenerationMxBean.getCollectionTime();
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // PS Mark Sweep
        System.out.println(String.format("%-30s : %-30s" ,"PSMarkSweepCollectionCount", markSweepCollectionCount));
        System.out.println(String.format("%-30s : %-30s" ,"PSMarkSweepCollectionTime", markSweepCollectionTime));
    }

    private void printPSScavengeInfo() {
        long scavengeCollectionCount = 0l;
        long scavengeCollectionTime = 0l;

        try {
            ObjectName scavengeObjectName = new ObjectName("java.lang:type=GarbageCollector,name=PS Scavenge");
            GarbageCollectorMXBean scavengeGenerationMxBean = JMX.newMBeanProxy(connection, scavengeObjectName, GarbageCollectorMXBean.class, true);

            scavengeCollectionCount = scavengeGenerationMxBean.getCollectionCount();
            scavengeCollectionTime = scavengeGenerationMxBean.getCollectionTime();
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // PS Scavenge
        System.out.println(String.format("%-30s : %-30s" ,"PSScavengeCollectionCount", scavengeCollectionCount));
        System.out.println(String.format("%-30s : %-30s" ,"PSScavengeCollectionTime", scavengeCollectionTime));
    }

    public void printVendorInfo() {
        String implementationVendor = "";
        String implementationVersion = "";

        try {
            ObjectName mBeanServerDelegateMBeanObjectName = new ObjectName("JMImplementation:type=MBeanServerDelegate");
            MBeanServerDelegateMBean mBeanServerDelegateMBean = JMX.newMBeanProxy(connection, mBeanServerDelegateMBeanObjectName, MBeanServerDelegateMBean.class, true);

            implementationVendor = mBeanServerDelegateMBean.getImplementationVendor();
            implementationVersion = mBeanServerDelegateMBean.getImplementationVersion();
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // vendor info
        System.out.println(String.format("%-30s : %-30s" ,"Vendor",implementationVendor));
        System.out.println(String.format("%-30s : %-30s" ,"Version",implementationVersion));;
    }

    private void printUpTime() {
        long upTime = 0;

        try {
            ObjectName runtimeObjectName = new ObjectName("java.lang:type=Runtime");
            RuntimeMXBean runtimeMXBean = JMX.newMBeanProxy(connection, runtimeObjectName, RuntimeMXBean.class, true);

            upTime = runtimeMXBean.getUptime();

            // up time is in ms. This does not make sense to me. Lets make it in seconds
            upTime = upTime / ( 1000 );
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        System.out.println(String.format("%-30s : %-30s", "UpTime", upTime));
    }

    public void printThreadCount()  {
        int daemonThreadCount = 0;
        int threadCount = 0;

        // sun open jdk version has ThreadMXBean in a different place
        // this causes ClassNotFoundException. In case JVM is Oracle skip this check
        if( vendor() == Vendor.ORACLE ) {
            try {
                ObjectName threadMXBeanObjectName = new ObjectName("java.lang:type=Threading");
                ThreadMXBean threadMXBean = JMX.newMBeanProxy(connection, threadMXBeanObjectName, ThreadMXBean.class, true);

                daemonThreadCount = threadMXBean.getDaemonThreadCount();
                threadCount = threadMXBean.getThreadCount();
            } catch( Exception ex ) {
                LOG.log(Level.WARNING, "ex", ex);
            }
        }

        // threads
        System.out.println(String.format("%-30s : %-30s" ,"DaemonThreads",daemonThreadCount));
        System.out.println(String.format("%-30s : %-30s" ,"TotalThreads",threadCount));
    }

    private void printClassLoading() {
        int loadedClassCount = 0;
        long unloadedClassCount = 0;

        try {
            ObjectName classLoadingObjectName = new ObjectName("java.lang:type=ClassLoading");
            ClassLoadingMXBean classLoadingMXBean = JMX.newMBeanProxy(connection, classLoadingObjectName, ClassLoadingMXBean.class, true);

            loadedClassCount = classLoadingMXBean.getLoadedClassCount();
            unloadedClassCount = classLoadingMXBean.getUnloadedClassCount();
        } catch( Exception ex ) {
            LOG.log(Level.WARNING, "ex", ex);
        }

        // ClassLoading
        System.out.println(String.format("%-30s : %-30s" ,"LoadedClassCount",loadedClassCount));
        System.out.println(String.format("%-30s : %-30s" ,"UnloadedClassCount",unloadedClassCount));
    }

    private static class AgentConfiguration {

        private  static final String CONFIG_LOCATION_PARAM = "--config";
        private  static final String DEBUG_PARAM = "--debug";
        private  static  final String DEBUG_FILE_PARAM = "--logfile";

        private String configLocation = null;
        private String logfile = null;
        private boolean debug = false;

        public void initArguments(String[] args) {
            for( String argument : args ) {

                if( argument.startsWith(CONFIG_LOCATION_PARAM)) {
                    // length + =
                    configLocation = argument.substring(CONFIG_LOCATION_PARAM.length() + 1);
                } else if(argument.equals(DEBUG_PARAM)) {
                    debug = true;
                } else if(argument.startsWith(DEBUG_FILE_PARAM)) {
                    logfile = argument.substring(DEBUG_FILE_PARAM.length() + 1);
                }
            }
        }

        public void initLoger() throws IOException {
            if(debug && logfile != null) {
                LOG.setLevel(Level.FINEST);

                // 50 MB in two files
                int limit = 50 * 1024 * 1024;
                FileHandler fileHandler = new FileHandler(logfile, limit, 2, true);
                fileHandler.setFormatter(new SimpleFormatter());
                LOG.addHandler(fileHandler);
            } else if(debug) {
                LOG.addHandler(new ConsoleHandler());
            } else {
                LOG.setLevel(Level.OFF);
            }

            LOG.setUseParentHandlers(false);
        }

        public String getConfigLocation() {
            return configLocation;
        }
    }

    public static void main(String[] args) throws IOException, MalformedObjectNameException, AttributeNotFoundException, MBeanException, ReflectionException, InstanceNotFoundException {

        AgentConfiguration agentConfiguration = new AgentConfiguration();
        agentConfiguration.initArguments(args);
        agentConfiguration.initLoger();

        ConfigLoader configLoader = new ConfigLoader(agentConfiguration.getConfigLocation());

        for( InstanceConfiguration configuration : configLoader.getConfigurations()) {
            String hostname = configuration.getHostname();
            Integer port = null;
            try {
                port = Integer.parseInt(configuration.getPort());
            } catch(NumberFormatException ex) {
                LOG.log(Level.WARNING, "ex", ex);
                System.exit(1);
            }

            try {
                ObserviumJvmUnixAgent agent = new ObserviumJvmUnixAgent(
                        "service:jmx:rmi:///jndi/rmi://" + hostname + ":" + port + "/jmxrmi",
                        configuration.getName());

                agent.init();

                agent.checkConfiguration();

                agent.printHeader();

                agent.printVendorInfo();

                agent.printUpTime();

                agent.printMemoryInfo();
                agent.printEdenSpace();
                agent.printPermGen();
                agent.printOldGeneration();

                agent.printG1OldGenerationInfo();
                agent.printG1YoungGenerationInfo();

                agent.printConcMarkSweepInfo();
                agent.printParNewInfo();
                agent.printCopyInfo();

                agent.printPSMarkSweepInfo();
                agent.printPSScavengeInfo();

                agent.printThreadCount();

                agent.printClassLoading();

                agent.close();
            } catch( Exception ex ) {
                LOG.log(Level.WARNING, "ex", ex);
            }
        }
    }

    private Vendor vendor() {
        String vendorString = System.getProperty("java.vendor");

        if( vendorString != null && vendorString.toLowerCase().contains("sun")) {
            return Vendor.SUN;
        } else {
            return Vendor.ORACLE;
        }
    }

    private enum Vendor {
        ORACLE, SUN;
    }
}
