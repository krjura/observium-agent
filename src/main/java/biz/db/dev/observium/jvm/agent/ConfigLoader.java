package biz.db.dev.observium.jvm.agent;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConfigLoader {

	private static final String SEPARATOR = "=";

	private static final String NAME = "jmx.name";
	private static final String HOSTNAME = "jmx.hostname";
	private static final String PORT = "jmx.port";

	private List<InstanceConfiguration> configurations;

	public ConfigLoader(String path) {
		this.configurations = new ArrayList<InstanceConfiguration>();

		loadConfiguration(path);
	}

	private void loadConfiguration(String configurationPath ) {
		File file = new File(configurationPath);

		if(!file.exists()){
			throw new IllegalArgumentException("configuration file not found");
		}

		FileReader fr = null;
		BufferedReader reader = null;

		try {
			fr = new FileReader(configurationPath);
			reader = new BufferedReader(fr);

			String line;

			InstanceConfiguration configuration = new InstanceConfiguration();
			configurations.add(configuration);

			while( (line = reader.readLine()) != null ) {

				// start new configuration
				if(line.trim().length() == 0) {
					configuration = new InstanceConfiguration();
					configurations.add(configuration);
				}

				setConfigurationParameter(line, configuration);
			}
		} catch( Exception ex ) {
			ex.printStackTrace();

			close(fr);
			close(reader);
		}
	}

	private void close(Closeable closeable ) {
		try {
			closeable.close();
		} catch( IOException ignored) {

		}
	}

	private void setConfigurationParameter(String line, InstanceConfiguration configuration) {
		String[] keyValue = line.split(SEPARATOR);
		if(keyValue.length != 2) {
			return;
		}

		String key = keyValue[0].trim();
		String value = keyValue[1].trim();

		if( key.equals(NAME) ) {
			configuration.setName(value);
		} else if( key.equals(HOSTNAME) ) {
			configuration.setHostname(value);
		} else if( key.equals(PORT) ) {
			configuration.setPort(value);
		}
	}

	public List<InstanceConfiguration> getConfigurations() {
		return configurations;
	}

	@Override
	public String toString() {
		return "ConfigLoader{" + "configurations=" + configurations + '}';
	}
}
